.PHONY: install clean

./bin/ougi: ./bin ./src/ougi
	cd src && cp ./ougi ../bin/ougi

./bin:
	mkdir bin

./src/ougi:
	$(MAKE) -C src

install: ./bin/ougi
	cp ./bin/ougi /usr/bin/ougi

clean:
	$(MAKE) clean -C src
