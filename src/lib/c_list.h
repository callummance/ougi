#ifndef C_LIST_INCLUDE
#define C_LIST_INCLUDE

#include <stdio.h>
#include <stdbool.h>

#define C_ELEM(e) ((struct comment*) e->content)

struct comment {
    char* content;
    int line_no;
    struct comment *next;
    struct comment *prev;
};

struct elem {
  void *content;
  struct elem *next;
  struct elem *prev;
};

struct list {
    struct elem head;
    struct elem tail;
};

struct file_comments_list {
  struct list content;
  char* filename;
};

void init_list(struct list*);
void destroy_files_list(struct list*);
void new_comment(struct list*, char*, int);
void remove_elem(struct elem*);
size_t list_len(struct list*);
bool list_empty(struct list*);
void insert_elem(struct list*, struct elem*);
void insert_item(struct list*, void*);
struct elem *prev_elem(struct elem *e);
struct elem *next_elem(struct elem *e);

#endif
