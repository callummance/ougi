#include "c_list.h"
#include <stdlib.h>
#include <string.h>

static void destroy_list(struct list*, void (*)(struct elem*));
static void remove_free_file(struct elem*);
static void remove_free_comment(struct elem*);

size_t list_len(struct list *l){
  size_t len = 0;
  struct elem *c;
  for (c = l->head.next; c!= &l->tail; c = c->next) ++len;
  return len;
}

bool list_empty(struct list *l){
  return l->head.next == &l->tail;
}

void init_list(struct list *newlist){
    newlist->head.next = &newlist->tail;
    newlist->head.prev = NULL;
    newlist->tail.prev = &newlist->head;
    newlist->tail.next = NULL;
}

void new_comment(struct list *list, char *content, int line){
    struct comment *comment = malloc(sizeof(struct comment));
    struct elem *elem = malloc(sizeof(struct elem));
    char *new_content = malloc(strlen(content)+1);
    comment->content = new_content;
    comment->line_no = line;
    strncpy(new_content, content, strlen(content)+1);
    free(content);
    elem->content = comment;
    insert_elem(list, elem);
}

void destroy_files_list(struct list *files){
  destroy_list(files, remove_free_file);
}

static void destroy_list(struct list *target, void (*remove)(struct elem*)){
  struct elem *e = target->head.next;
  while (e != &target->tail){
    struct elem *next = e->next;
    remove(e);
    e = next;
  }
}


void remove_elem(struct elem *elem){
  struct elem *prev = elem->prev;
  struct elem *next = elem->next;

  prev->next = next;
  next->prev = prev;
}

static void remove_free_file(struct elem *elem){
  struct file_comments_list *payload = (struct file_comments_list*) elem->content;
  destroy_list(&payload->content, &remove_free_comment);
  free(payload->filename);
  free(payload);
  free(elem);
}

static void remove_free_comment(struct elem *elem){
  remove_elem(elem);
  free(C_ELEM(elem)->content);
  free(elem->content);
  free(elem);
}

void insert_elem(struct list *list, struct elem *elem){
  struct elem *e;
  for (e = list->head.next; e != &list->tail; e = e->next){
    if (C_ELEM(elem)->line_no > C_ELEM(e)->line_no) {
      struct elem *before = e;
      struct elem *after = before->next;

      before->next = elem;
      elem->prev = before;

      elem->next = after;
      after->prev = elem;
      return;
    }
  }
  list->tail.prev->next = elem;
  elem->prev = (list->tail.prev);

  elem->next = &list->tail;
  list->tail.prev = elem;
}

void insert_item(struct list *l, void *item){
  struct elem *e = (struct elem*) malloc(sizeof(struct elem));
  e->content = item;
  insert_elem(l, e);
}

struct elem *next_elem(struct elem *e){
  return e->next;
}
struct elem *prev_elem(struct elem *e){
  return e->prev;
}
