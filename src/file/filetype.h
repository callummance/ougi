#ifndef FILETYPE_HEADER
#define FILETYPE_HEADER

#include <inttypes.h>

enum comment_style {
  INVALID_EXT,
  C_STYLE,
  PYTHON_STYLE,
  HASKELL_STYLE,
  XML_STYLE,
  MATLAB_STYLE,
};

char *get_extension(char*);
uint64_t hash_extension(char*);
enum comment_style detect_type(char*);

#endif
