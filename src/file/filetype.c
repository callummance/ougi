#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "filetype.h"

#define MAX_HASH 9
#define CHAR_BITS 7
#define SHIFT_CHAR(c, n) ((uint64_t) ((c << 1) >> 1)) << (n * CHAR_BITS)

char *get_extension(char *filename){
  char *filename_cpy = strndup(filename, strlen(filename));
  char *saveptr = NULL;
  char *prev = NULL;
  char *cur = strtok_r(filename_cpy, ".", &saveptr);
  while((cur = strtok_r(NULL, ".", &saveptr))) prev = cur;
  if (!prev) {
    free(filename_cpy);
    return NULL;
  }
  char *res = strndup(prev, strlen(prev));
  free(filename_cpy);
  return res;
}

uint64_t hash_extension(char *ext){
  if (!ext) return 0;
  uint64_t res = 0;
  size_t chars_to_hash = strlen(ext) < MAX_HASH ? strlen(ext) : MAX_HASH;
  int i;
  for (i = 0; i < chars_to_hash; ++i){
    res = res | SHIFT_CHAR(ext[strlen(ext) - 1 - i], i);
  }
  return res;
}

enum comment_style detect_type (char *file){
  char *ext = get_extension(file);
  uint64_t filetype = hash_extension(ext);
  free(ext);
  switch (filetype) {
    case 0x0:
      return INVALID_EXT;
    case 0x63:
      //.c files
      return C_STYLE;
    case 0x68:
      //.h files
      return C_STYLE;
    case 0x6C:
      //.l files
      return INVALID_EXT;
    case 0x6D:
      //.m files
      return C_STYLE;
    case 0x79:
      //.y files
      return INVALID_EXT;
    case 0x31F3:
      //.cs files
      return C_STYLE;
    case 0x3573:
      //.js files
      return C_STYLE;
    case 0x3973:
      //.rs files
      return C_STYLE;
    case 0x18F870:
      //.cpp files
      return C_STYLE;
    case 0x1A3870:
      //.hpp files
      return C_STYLE;
    case 0x1C3470:
      //.php files
      return C_STYLE;
    case 0xD587B61:
      //.java files
      return C_STYLE;
    case 0xE5B34E2:
      //.rlib files
      return C_STYLE;
    case 0xE7879F3:
      //.sass files
      return C_STYLE;
    case 0x73EFA7374:
      //.swift files
      return C_STYLE;
    default:
      return INVALID_EXT;
      //printf("Found an unknown file of type %lu with ext %s\n", filetype, ext);
  }
  return 0;
}
