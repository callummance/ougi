#ifndef ENUMERATE_DIR_HEADER
#define ENUMERATE_DIR_HEADER

void print_fils(char*);
void run_on_files(char*, void (*)(char*, void*), void*);

#endif
