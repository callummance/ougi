#include <sys/types.h>
#include <stdlib.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>

#include "../lib/c_list.h"


void print_files(char *dir_name){
  DIR *target_dir = opendir(dir_name);
  struct dirent *dir_entry = readdir(target_dir);
  while (dir_entry) {
    char *file_type = dir_entry->d_type == DT_REG ? "file" : "directory";
    printf("Found %s: %s\n", file_type, dir_entry->d_name);
    dir_entry = readdir(target_dir);
  }
}

void run_on_files(char *dir_name, void (*f)(char*, void*), void *aux){
  DIR *target_dir = opendir(dir_name);
  struct dirent *dir_entry = readdir(target_dir);
  struct list *files_list = (struct list*) aux;
  while (dir_entry) {
    if (dir_entry->d_name[0] == '.'){
      dir_entry = readdir(target_dir);
      continue;
    }

    //Contstruct new full file path
    size_t name_size = strlen(dir_name) + 1;
    size_t subname_size = strlen(dir_entry->d_name) + 1;
    char* fq_dir = (char*) malloc(name_size + subname_size);
    strncpy(fq_dir, dir_name, name_size);
    fq_dir[name_size-1] = '/'; fq_dir[name_size] = '\0';
    strncat(fq_dir, dir_entry->d_name, subname_size);

    switch (dir_entry->d_type) {
    case DT_REG:;
      //Create a new list and insert it into the root list
      struct file_comments_list *file_list = (struct file_comments_list*) malloc(sizeof(struct file_comments_list));
      init_list(&file_list->content);
      file_list->filename = fq_dir;
      insert_item(files_list, file_list);
      //Run the given function on a regular file
      f(fq_dir, &file_list->content);
      break;
    case DT_DIR:;
      //Recursively call run_on_files
      run_on_files(fq_dir, f, files_list);
      free(fq_dir);
      break;
    default:
      printf("This file type has not yet been handled\n");
    }
    dir_entry = readdir(target_dir);
  }
  closedir(target_dir);
}
