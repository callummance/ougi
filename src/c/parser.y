%{
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include "../lib/c_list.h"

    extern int cclex();
    extern int cclineno;
    extern void ccerror(struct list*, char*);
    extern int last_comment_start;
    extern FILE *ccin;

    char *concat_strings_space(char*, char*);
    void create_list(void);

%}

%union {
    int     int_val;
    char*   str_val;
}

%token <int_val>    LINECOMMENT ENDBLOCKCOMMENT BLOCKCOMMENT NEWLINE TODODECL
%token <str_val>    TEXT

%type <str_val> text_lines


%destructor { free($$); } TEXT text_lines

%start docstart_node

%locations
%parse-param {struct list *todos}
%define api.prefix {cc}

%%

docstart_node:
          root_node                 {last_comment_start = 0; cclineno = 0;}
         ;

root_node:          
          content_node root_node
         |content_node
         |
         ;

content_node:
          comment_node
         |text_lines
         ;

text_lines:
          TEXT                        {$$ = $1;}
         |text_lines new_lines TEXT   {$$ = concat_strings_space($1, $3);}
         ;

new_lines:
         NEWLINE
        |new_lines NEWLINE
        ;

comment_node:
          BLOCKCOMMENT TODODECL text_lines ENDBLOCKCOMMENT    {new_comment(todos, $3, last_comment_start);}
         |BLOCKCOMMENT ENDBLOCKCOMMENT
         |LINECOMMENT TODODECL TEXT NEWLINE                   {new_comment(todos, $3, last_comment_start);}
         |LINECOMMENT NEWLINE
         ;

%%


char* concat_strings_space(char *str1, char *str2) {
    size_t len1 = strlen(str1);
    size_t len2 = strlen(str2);

    char *result = malloc(len1 + len2 + 2); //1 for null, 1 for space.

    strncpy(result, str1, len1);
    result[len1] = ' ';
    strncpy(result+len1+1, str2, len2+1);
    result[len1+len2] = '\0';

    free(str1);
    free(str2);

    return result;
}
