%{

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>

    #include "../lib/c_list.h"
    #include "parser.tab.h"

    void cerror(struct list*, char*);
    int cparse(struct list*);

    int last_comment_start = 0;
%}

%option noyywrap
%option yylineno
%option prefix="cc"
%x LINE_COMMENT BLOCK_COMMENT LINE_TODO BLOCK_TODO

%%

<INITIAL>"//"[ \t]*                             {
                                                   last_comment_start = cclineno; 
                                                   BEGIN(LINE_COMMENT);
                                                   return LINECOMMENT;
                                                }
<INITIAL>"/*"[ \t]*                             {
                                                   last_comment_start = cclineno;
                                                   BEGIN(BLOCK_COMMENT);
                                                   return BLOCKCOMMENT;
                                                }
<BLOCK_COMMENT,BLOCK_TODO>[ \n]*[ \t]*"*/"[ \n]* {BEGIN(INITIAL); return ENDBLOCKCOMMENT;}
<LINE_COMMENT,LINE_TODO>[ \t]*[\n][ \t]*        {BEGIN(INITIAL); return NEWLINE;}
<BLOCK_TODO>[\t]*[\n][ \t]*                     {return NEWLINE;}
<BLOCK_COMMENT>"TODO"[:.]?[ \t]*                {BEGIN(BLOCK_TODO); return TODODECL;}
<LINE_COMMENT>"TODO"[:.]?[ \t]*                 {BEGIN(LINE_TODO); return TODODECL;}
<BLOCK_TODO>[ \t]*["*"]*[ \t]*
<LINE_TODO,BLOCK_TODO>[^*\n/]*                 {
                                                   char *res = malloc(yyleng+1);
                                                   strncpy(res, yytext ,yyleng+1);
                                                   cclval.str_val = res;
                                                   return TEXT;
                                                }
<INITIAL,BLOCK_COMMENT,LINE_COMMENT>.   
<INITIAL,BLOCK_COMMENT>[\n]*
%%

void ccerror(struct list *c, char *s) {
    printf("PARSE ERROR: PANIC! on line %d\n Message: %s\n", cclineno, s);
    exit(-1);
}

