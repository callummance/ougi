#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "lib/c_list.h"
#include "file/filetype.h"
#include "file/enumerate_dir.h"
#include "c/parser.tab.h"

#define MAX_EXT 16

extern FILE *ccin;

static void parse_c_file(char*, struct list*);
void print_file_comments(char*, void*);
void results_to_stdout(struct list*);

int main(int argc, char* argv[]){
  //Get target directory from command line
  char* target_dir = argc-1 ? argv[1] : ".";
  struct list todos_file;
  init_list(&todos_file);
  //TODO: Change run_on_files to also take a character array pointer
  run_on_files(target_dir, &print_file_comments, &todos_file);
  results_to_stdout(&todos_file);

  //Clean up memory
  destroy_files_list(&todos_file);
}

void results_to_stdout(struct list *l){
  struct elem *e;
  for (e = l->head.next; e != &l->tail; e = next_elem(e)) {
    struct file_comments_list *f = (struct file_comments_list*) e->content;
    size_t list_size = list_len(&f->content);
    char* file = f->filename;
    if (list_size) printf("Found %d TODO(s) in file %s\n----------------------------------\n", (int) list_size, file);

    struct elem *c;
    for (c = f->content.head.next; c != &f->content.tail; c = next_elem(c)) {
      printf ("TODO found on line %d: %s\n", C_ELEM(c)->line_no, C_ELEM(c)->content);
    }
    if (list_size) printf("\n\n");
  }
}

void print_file_comments(char *file, void *aux){
  //Fetch and clean the list
  struct list *todos_file = (struct list*) aux;

  //Select and run the relevant parser
  enum comment_style style = detect_type(file);
  switch(style){
    case INVALID_EXT:
      return;
    case C_STYLE:
      parse_c_file(file, todos_file);
      break;
    default:
      printf("Not yet implemented");
      return;
  }
}


static void parse_c_file(char* file_name, struct list *list){
  FILE *parse_target = fopen(file_name, "r");
  if (!parse_target) return;
  ccin = parse_target;
  do {
    ccparse(list);
  } while(!feof(ccin));
  fclose(parse_target);
}

