# Ougi #

A TODO extractor for source code files on Linux systems, ver. 0.01

### Features ###

######Current Features:########
* Scans C source files for comments beginning with 'TODO:'

######Planned Features:######
* Support for other languages
* Automatic source language detection
* Monitoring of files
* Automatic source directory detection


### How do I get set up? ###

* Clone the repo
* Run 'make'


### Any Questions ###
* Email me at [callummance@gmail.com](mailto:callummance@gmail.com)